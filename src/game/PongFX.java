package game;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class PongFX extends Application {
	public Scene scene;
	public Group root;
	public int gameHeight;
	public int gameWidth;

	public Rectangle player1;
	public Rectangle player2;
	public Circle ball;
	
	public boolean ballUp;
	public boolean ballLeft;
	public int ballDx;
	public int ballDy;

	public boolean player1Up;
	public boolean player1Down;

	public boolean player2Up;
	public boolean player2Down;

	@Override
	public void start(Stage primaryStage) {
		initWindows();
		initObjects();
		initEventHandlers();
		startAnimation();
		setStage(primaryStage);
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	public void initEventHandlers() {
		scene.setOnKeyPressed(event -> {
			switch(event.getCode()) {
			case W:
				player1Up = true;
				break;
			case S:
				player1Down = true;
				break;
			case UP:
				player2Up = true;
				break;
			case DOWN:
				player2Down = true;
				break;
			}
		});
		scene.setOnKeyReleased(event -> {
			switch(event.getCode()) {
			case W:
				player1Up = false;
				break;
			case S:
				player1Down = false;
				break;
			case UP:
				player2Up = false;
				break;
			case DOWN:
				player2Down = false;
				break;
			}
		});

	}


	public void startAnimation() {
		AnimationTimer timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				if (player1Up && player1.getY() > 0) {
					player1.setY(player1.getY() - 10);
				}
				if (player1Down && player1.getY() + player1.getHeight() < gameHeight) {
					player1.setY(player1.getY() + 10);
				}
				if (player2Up && player2.getY() > 0) {
					player2.setY(player2.getY() - 10);
				}
				if (player2Down && player2.getY() + player2.getHeight() < gameHeight) {
					player2.setY(player2.getY() + 10);
				}
			}
			
		};
		timer.start();
	}

	private void initWindows() {
		this.gameHeight = 500;
		this.gameWidth = 900;
		this.root = new Group();
		this.scene = new Scene(root, gameWidth, gameHeight);		
	}

	public void initObjects() {
		player1 = new Rectangle(50, 150);
		player1.setFill(Color.BLUE);
		root.getChildren().add(player1);
		player1.setX(10);
		player1.setY((500 / 2) - player1.getHeight() / 2);


		player2 = new Rectangle(50, 150);
		player2.setFill(Color.ORANGE);
		root.getChildren().add(player2);
		player2.setX((900 - player2.getWidth()) - 10);
		player2.setY((500 / 2) - player2.getHeight() / 2);
		
		
		ball = new Circle(25);
		ballDy = 3;
		ballDx = 4;
		ball.setFill(Color.GRAY);
		root.getChildren().add(ball);
		ball.setCenterX(gameWidth / 2);
		ball.setCenterY(gameHeight / 2);
	}

	private void setStage(Stage primaryStage) {
		primaryStage.setScene(this.scene);
		primaryStage.show();
	}

}